<?php
return [
    'emails' => [
        'spam_protection'   => '电子邮件没发送。 垃圾邮件规则',
        'sent_successfully' => '电子邮件发送成功',
        'unable_to_send'    => '无法发送该电子邮件',

        'request' => [
            'email_required' => 'Email 是必填的',
            'email_max'      => 'Email 字母最大长度是255个字符',
            'email_email'    => 'Email 错误的',

            'text_required' => '文本是必填的',
            'text_min'      => '文本 字母最小长度是10个字符',
        ],
    ],
    'transaction_types' => [
        'enter'      => '充值',
        'withdraw'   => '提取资金',
        'bonus'      => '奖金',
        'partner'    => '合作伙伴佣金',
        'dividend'   => '存款收益',
        'create_dep' => '创建存款',
        'close_dep'  => '关闭存款',
        'penalty'    => '罚款',
    ],

];
