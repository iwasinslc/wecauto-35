<?php
return [
    'order_created' =>'Заказ #:id :amount :currency был успешно создан',
    'order_closed' => 'Заказ #:id :amount :currency был закрыт из-за отсутствия средств на балансе',
    'sale' =>'Продажа :amount :currency прошла успешно',
    'transfer_confirmed' => 'Транзакция подтверждена по ссылке в емейле',
    'withdraw_confirmed' => 'Вывод средств подтвержден по ссылке в емейле. Запрос отправлен администрации.',
    'purchase'=>'Покупка :amount :currency прошла успешно',
    'partner_accrue'=>'Вы только что получили партнерскую комиссию :amount :currency от юзера :login, на уровне :level',
    'wallet_refiled'=>'Ваш кошелек был пополнен на :amount :currency',
    'rejected_withdrawal'=>'Ваш вывод на сумму :amount :currency был отменен.',
    'approved_withdrawal'=>'Ваш вывод на сумму :amount :currency был подтвержден.',
    'new_partner'=>'У вас новый партнер :login на уровне :level',
    'parking_bonus'=>'Бонус за паркинг :amount :currency',
    'licence_cash_back'=> 'Кешбек за покупку лицензии :amount :currency'
];