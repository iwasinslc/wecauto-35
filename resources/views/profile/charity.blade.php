@extends('layouts.profile')
@section('title', str_replace(' ', '', __('Charity')))
@section('content')

    <section class="charity section-border">
        <div class="container">

            <h3 class="lk-title">{{ str_replace(' ', '', __('Charity'))}}
            </h3>
            <div class="charity__block">
                <div class="charity__row">
                    <div class="charity__col">
                        <div class="typography">
                            <h3 class="color-warning">{{ __('About the Foundation')}}<br>{{__('“Pure Hearts”') }}</h3>
                            <p>{{__('We see that charity is the donation of huge amounts of money, but in fact, even $5 directed to charitable purposes can make a big difference.')}} </p>
                            <p>{{__("Imagine what it's like for a child who can't meet their basic needs for delicious food, clean clothes, and the most basic children's toys. Think about it for a second. Today you can give him a helping hand and do what you think is a small thing, give such a child a simple human happiness. Having received financial assistance from you, this little man will appreciate first of all the attention shown and will get hope for finding a better life.")}}</p>
                            <p>{{__('The charity project "Pure Hearts" provides assistance to orphaned children and newborns in hospitals that were abandoned by their parents. We work exclusively with verified, officially registered organizations, and always receive reports on the expenditure of donated funds in writing + photos that are published on our channels in:')}} </p>

                        </div>
                    </div>
                    <div class="charity__col">
                        <div class="donate">
                            <div class="donate__top">
                                <p class="donate__title">{{__('Donate funds')}}
                                </p>
                                <form class="balance-form" method="POST" action="{{ route('profile.charity.store') }}">
                                    {{ csrf_field() }}
                                    <div class="balance-form__field">
                                        <label>{{__('Amount')}}:</label>
                                        <input type="text" name="amount"/>
                                    </div>
                                    <div class="balance-form__field">
                                        <label>{{__('Currency')}}:</label>
                                        <label class="select">
                                            <select id="wallet_id" name="wallet_id" class="" autofocus>
                                                <option value="{{getUserWallet('USD', 'perfectmoney')['id']}}">USD</option>
                                                <option value="{{getUserWallet('WEC')['id']}}">WEC</option>

                                            </select>
                                        </label>
                                    </div>
                                    <div class="balance-form__bottom">
                                        <button class="btn btn--warning btn--size-lg"><span>{{__('Donate funds')}}</span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="donate__bottom">
                                <ul class="social-list-poly">
                                    <li>
                                        <div class="social-list-poly__icon">
                                            <svg class="svg-icon">
                                                <use href="/assets/icons/sprite.svg#icon-telegram-bold"></use>
                                            </svg>
                                        </div>
                                        <div class="social-list-poly__content"><a href="https://t.me/chistoe_serdce">Telegram Channel</a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="social-list-poly__icon">
                                            <svg class="svg-icon">
                                                <use href="/assets/icons/sprite.svg#icon-instagram"></use>
                                            </svg>
                                        </div>
                                        <div class="social-list-poly__content"><a href="https://instagram.com/chistoe_serdce_wtp">Instagram</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- /.card -->
@endsection

