@extends('layouts.profile')
@section('title',  __('Add review'))
@section('content')
    <section class="section-border">
        <div class="container">

                <div class="form-section form-section--border-bottom">
                    <div class="lk-title">{{__('Add review')}}
                    </div>
                    <div class="field field--row">
                        <label>{{__('Type')}}</label>
                        <label class="select">
                            <select id="type">
                                <option value="0">{{__('Text')}}</option>
                                <option value="1">{{__('Video')}}</option>
                            </select>
                        </label>
                    </div>
                    <form method="POST" action="{{route('profile.reviews.store')}}" id="form_1" style="display: none">
                        {{csrf_field()}}
                        <input type="hidden" name="type" value="1">
                        <div class="field field--row">
                            <label>{{__('URL')}} Youtube</label>
                            <input  name="video" type="text">
                        </div>
                        <div class="field field--row">
                            <label>{{__('Title')}}</label>
                            <input  name="name" type="text">
                        </div>
                    </form>
                    <form method="POST" action="{{route('profile.reviews.store')}}"  id="form_0">
                        {{csrf_field()}}
                        <input type="hidden" name="type" value="0">
                        <div class="field field--row">
                            <label>{{__('Title')}}</label>
                            <input name="name" type="text">
                        </div>
                        <div class="field field--row">
                            <label>{{__('Text')}}</label>
                            <textarea  name="text"></textarea>
                        </div>
                    </form>

                </div>
                <div class="form-section">
                    <button form="form_0" id="but" type="submit" class="btn btn--warning btn--size-lg">{{__('Add review')}}
                    </button>
                </div>

        </div>
    </section>
    <!-- /.card -->
@endsection

@push('load-scripts')

    <script>
        $('#type').change(function () {
            var type = $(this).val();
            $('#but').attr('form','form_'+type);
            $('form').hide();
            $('#form_'+type).show();


        })
    </script>
@endpush

