@extends('admin.layouts.app')
@section('title')
    {{ __('Email marketing') }}
@endsection
@section('breadcrumbs')
    <li> {{ __('Email marketing') }}</li>
@endsection
@section('content')
    <!-- row -->
    <div class="row">
        <!-- col -->
        <div class="col-md-12">

            <!-- tile -->
            <section class="tile">

                <!-- tile header -->
                <div class="tile-header dvd dvd-btm">
                    <h1 class="custom-font">{{ __('Email marketing') }}</h1>
                    <ul class="controls">
                        <li>
                            <a role="button" class="tile-fullscreen">
                                <i class="fa fa-expand"></i> {{ __('Fullscreen') }}
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /tile header -->

                <!-- tile body -->
                <div class="tile-body">

                    <form class="form-horizontal" method="POST"
                          action="{{ route('admin.email_marketing.handle') }}">
                        {{ csrf_field() }}
                        <fieldset>
                            <!-- Text input-->
                            <div class="form-group">
                                <div class="col-md-6">
                                    <input type="text" name="title" class="form-control" placeholder="Title .." value="{{ isset($title) ? $title : '' }}">
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <div class="col-md-6">
                                    <select class="form-control" name="send_to">
                                        <option value="admins">{{ __('only admins (to check email)') }}</option>
                                        <option value="all">{{ __('all users') }}</option>
                                        <option value="with_deposits">{{ __('with active deposits') }}</option>
                                        <option value="without_deposits">{{ __('without deposits') }}</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Text input-->
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea class="summernote" name="content">{{ isset($content) ? $content : '' }}</textarea>
                                    <span class="help-block"> </span>
                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <div class="col-md-3">
                                    <button class="btn btn-success large sure">{{ __('Start mailing') }}</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <!-- /tile body -->

            </section>
            <!-- /tile -->

        </div>
        <!-- /col -->
    </div>
    <!-- /row -->
@endsection

@push('load-scripts')
    <script>
        $(document).ready(function() {
            $('.summernote').summernote({
                placeholder: 'Email content',
                tabsize: 2,
                height: 300,
            });
        });
    </script>
@endpush
