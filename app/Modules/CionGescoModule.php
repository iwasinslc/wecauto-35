<?php
namespace App\Modules;

use GuzzleHttp\Client;

/**
 * Class FixerModule
 * @package App\Modules
 */
class CionGescoModule
{
    /** @var string $api */
    private $api = 'https://api.coingecko.com/api/v3/';

    /**
     * @param string $method
     * @param string|null $type
     * @param array|null $data
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
     */
    public function sendRequest(string $method, string $type=null, array $data=null)
    {
        if (null === $type) {
            $type = 'GET';
        }

        if (null === $data) {
            $data = [];
        }

        $client   = new Client();
        $baseUrl  = $this->api;
        $headers  = [
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];
        $verify   = config('app.env') == 'production' ? true : false;
        $params   = [
            'headers' => $headers,
            'verify'  => $verify,
        ];

        if (!empty($data)) {
            $params['form_params'] = $data;
        }

        try {
            $response = $client->request($type, $baseUrl.$method, $params);
        } catch (\Exception $e) {
            throw new \Exception('CionGesco API request is failed. '.$e->getMessage());
        }

        if ($response->getStatusCode() !== 200) {
            throw new \Exception('CionGesco API response status is '.$response->getStatusCode().' for method '.$method);
        }

        $body = json_decode($response->getBody()->getContents(), true);

        return $body;
    }

    /**
     * @param $from
     * @param array $symbols
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
     */
    public static function getRate($coins, $currencies)
    {

        try {
            $response = (new CionGescoModule())->sendRequest('/simple/price?ids='.$coins.'&vs_currencies='.$currencies, 'GET');
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $response;
    }
}