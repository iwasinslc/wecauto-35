<?php
namespace App\Observers;

use App\Models\TransactionType;
use App\Models\Withdraw;

/**
 * Class WithdrawObserver
 * @package App\Observers
 */
class WithdrawObserver
{
    /**
     * @param Withdraw $transaction
     * @return array
     * @throws
     */
    private function getCacheKeys(Withdraw $transaction): array
    {
        return [
            'i.totalWithdrew',
            'a.withdrawRequestsCount',
            'usersWithdrawals.user-' . $transaction->user_id
        ];
    }

    /**
     * @param Withdraw $transaction
     * @return array
     * @throws \Exception
     */
    private function getCacheTags(Withdraw $transaction): array
    {
        if (null == $transaction->user_id) {
            return [];
        }

        $tags = [
            'totalByTransactionType',
            'userBalancesByCurrency.' . $transaction->user_id,
            'userWithdrawOperations.' . $transaction->user_id,
            'userTotalWithdrawn.' . $transaction->user_id,
            'userTotalByTransactionType.' . $transaction->user_id . '.' . TransactionType::TRANSACTION_TYPE_WITHDRAW,
            'lastWithdrawals'
        ];

        return $tags;
    }

    /**
     * Listen to the Transaction created event.
     *
     * @param Withdraw $transaction
     * @return void
     * @throws
     */
    public function created(Withdraw $transaction)
    {
        clearCacheByArray($this->getCacheKeys($transaction));
        clearCacheByTags($this->getCacheTags($transaction));
    }

    /**
     * Listen to the Transaction deleting event.
     *
     * @param Withdraw $transaction
     * @return void
     * @throws
     */
    public function deleted(Withdraw $transaction)
    {
        clearCacheByArray($this->getCacheKeys($transaction));
        clearCacheByTags($this->getCacheTags($transaction));
    }

    /**
     * Listen to the Transaction updating event.
     *
     * @param Withdraw $transaction
     * @return void
     * @throws
     */
    public function updated(Withdraw $transaction)
    {
        clearCacheByArray($this->getCacheKeys($transaction));
        clearCacheByTags($this->getCacheTags($transaction));
    }
}