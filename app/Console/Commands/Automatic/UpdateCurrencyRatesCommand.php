<?php
namespace App\Console\Commands\Automatic;

use App\Models\Setting;
use App\Modules\CionGescoModule;
use App\Modules\FixerModule;
use App\Modules\LivecoinModule;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

/**
 * Class UpdateCurrencyRatesCommand
 * @package App\Console\Commands\Automatic
 */
class UpdateCurrencyRatesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:currency_rates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all currency rates to RUB.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException|\Exception
     */
    public function handle()
    {
//        $currencies = [
//            'BTC',
//            'USD',
//        ];
//
//        $this->info('work with FIXER');
//        $this->warn('API key: '.env('FIXER_ACCESS_KEY'));
//
//        foreach($currencies as $currency) {
//            $rates = FixerModule::getRate($currency, ['RUB']);
//            $this->info(print_r($rates,true));
//
//            if (isset($rates->rates->RUB)) {
//                $k = strtolower($currency).'_to_rub';
//                $v = $rates->rates->RUB;
//                Setting::setValue($k, $v);
//                $this->info($k.' -> '.$v);
//            }
//        }
//
//        $this->line('-----------');
//
//        foreach($currencies as $currency) {
//            $rates = FixerModule::getRate('RUB', [$currency]);
//            $this->info(print_r($rates,true));
//
//            if (isset($rates->rates->$currency)) {
//                $k = 'rub_to_'.strtolower($currency);
//                $v = $rates->rates->$currency;
//                Setting::setValue($k, $v);
//                $this->info($k.' -> '.$v);
//            }
//        }
//
//
//
//        $currencies = [
//            'BTC'
//        ];
//
//        $this->info('work with FIXER');
//        $this->warn('API key: '.env('FIXER_ACCESS_KEY'));
//
//        foreach($currencies as $currency) {
//            $rates = FixerModule::getRate($currency, ['USD']);
//            $this->info(print_r($rates,true));
//
//            if (isset($rates->rates->USD)) {
//                $k = strtolower($currency).'_to_usd';
//                $v = $rates->rates->USD;
//                Setting::setValue($k, $v);
//                $this->info($k.' -> '.$v);
//            }
//        }
//
//        $this->line('-----------');
//
//        foreach($currencies as $currency) {
//            $rates = FixerModule::getRate('USD', [$currency]);
//            $this->info(print_r($rates,true));
//
//            if (isset($rates->rates->$currency)) {
//                $k = 'usd_to_'.strtolower($currency);
//                $v = $rates->rates->$currency;
//                Setting::setValue($k, $v);
//                $this->info($k.' -> '.$v);
//            }
//        }
//
//        // ---------------------------------------------
//
//        $pairs = [
//            'DOGE' => 'USD',
//            'LTC' => 'USD',
//            'BTC' => 'USD',
//            'ETH' => 'USD',
//            'USDT' => 'USD',
//        ];
//
//        $this->info('work with LIVECOIN');
//
//        foreach ($pairs as $key => $val) {
//            $rate = LivecoinModule::getRate($key.'/'.$val);
//
//            $this->info(print_r($rate,true));
//
//            if (isset($rate->last)) {
//                $k = strtolower($key).'_to_'.strtolower($val);
//                $k2 = strtolower($key).'_to_rub';
//                $v = $rate->last;
//                $v2 = convertUsdToRub($v);
//                Setting::setValue($k, $v);
//                Setting::setValue($k2, $v2);
//                $this->info($k.' -> '.$v);
//            }
//
//            $rate2 = LivecoinModule::getRate($val.'/'.$key);
//
//            if (isset($rate2->last)) {
//                $k = strtolower($val).'_to_'.strtolower($key);
//                $v = $rate2->last;
//                Setting::setValue($k, $v);
//
//                $this->info($k.' -> '.$v);
//            }
//            else
//            {
//                $k = strtolower($val).'_to_'.strtolower($key);
//                $v = 1/$rate->last;
//                Setting::setValue($k, $v);
//
//                $this->info($k.' -> '.$v);
//            }
//        }

        $coins = [
//            'prizm'=>'PZM',
            'ethereum'=>'ETH',
            'bitcoin'=>'BTC',
            'bip'=>'BIP',
        ];
        $currencies = 'usd';
        $rates = CionGescoModule::getRate(implode(',', array_keys($coins)), $currencies);
        foreach ($rates as $key => $rate)
        {
            Setting::setValue(strtolower($coins[$key]).'_to_usd', $rate['usd']-$rate['usd']*3*0.01);
        }


        $acc = $this->sendRequest('https://crypto-accelerator.io/', 'api/price');

        Setting::setValue('acc_to_usd',  $acc);
        Setting::setValue('usd_to_acc',  1/$acc);


        $gnt = $this->sendRequest('https://prizmwector.com/', 'api/price');

        Setting::setValue('gnt_to_usd',  $gnt);
        Setting::setValue('usd_to_gnt',  1/$gnt);


        $acc_wec =$acc*rate('USD', 'WEC');

        Setting::setValue('acc_to_wec',  $acc_wec);


    }


    public function sendRequest($url, string $method, string $type=null, array $data=null)
    {
        if (null === $type) {
            $type = 'GET';
        }

        if (null === $data) {
            $data = [];
        }

        $client   = new Client();
        $baseUrl  = $url;
        $headers  = [
            'Content-Type' => 'application/x-www-form-urlencoded'
        ];
        $verify   = config('app.env') == 'production' ? true : false;
        $params   = [
            'headers' => $headers,
            'verify'  => $verify,
        ];

        if (!empty($data)) {
            $params['form_params'] = $data;
        }

        try {
            $response = $client->request($type, $baseUrl.$method, $params);
        } catch (\Exception $e) {
            throw new \Exception('CionGesco API request is failed. '.$e->getMessage());
        }

        if ($response->getStatusCode() !== 200) {
            throw new \Exception('CionGesco API response status is '.$response->getStatusCode().' for method '.$method);
        }

        $body = json_decode($response->getBody()->getContents(), true);

        return $body;
    }
}
