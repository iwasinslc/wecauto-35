<?php
namespace App\Http\Controllers\Profile;

use App\Events\ExchangeAllOperations;
use App\Events\Message;
use App\Http\Controllers\Controller;
use App\Http\Requests\DatatablesRequest;
use \App\Http\Requests\Exchange\RequestExchange;

use App\Http\Requests\Exchange\RequestExchangeCommon;
use App\Models\Currency;
use App\Models\ExchangeOrder;
use App\Models\OrderPiece;
use App\Models\OrderRequest;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\TransactionType;
use App\Models\Wallet;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

/**
 * Class ExchangeController
 * @package App\Http\Controllers\Profile
 */
class ExchangeController extends Controller
{
    public function index()
    {
        $code='WEC';
        $mainCode = 'FST';

        return view('profile.exchange', [
            'code'=>$code,
            'mainCode'=>$mainCode
        ]);
    }




    public function currency($mainCode = 'FST', $code='WEC')
    {
        if ($code !== 'WEC')
        {
            return back()->with('error', __('Error'));
        }

        if ($mainCode !== 'FST')
        {
            return back()->with('error', __('Error'));
        }



        return view('profile.exchange', [

            'code'=>$code,
            'mainCode'=>$mainCode
        ]);
    }

    public function wec_exchange($mainCode = 'WEC', $code='USD')
    {

//
//        if (!\user()->hasRole('root'))
//        {
//            return back()->with('error', __('Error'));
//        }

        return view('profile.exchange', [

            'code'=>$code,
            'mainCode'=>$mainCode
        ]);
    }

    public function buy_orders($mainCode, $code)
    {
        $orders = cache()->tags('OrdersTableCommon')->remember('OrdersTableCommon.Buy.'.$code.$mainCode,  now()->addMinutes(10), function () use ($code, $mainCode)   {
            return ExchangeOrder::query()->select(DB::raw('rate, sum(amount) as sum'))
                ->where('type', ExchangeOrder::TYPE_BUY)
                ->where('currency_id', Currency::getByCode($code)->id)
                ->where('main_currency_id', Currency::getByCode($mainCode)->id)
                ->active()
                ->groupBy('rate')
                ->orderBy('rate', 'desc')
                ->get();
        });

        return DataTables::of($orders)
            ->addColumn('num_rate', function (ExchangeOrder $order) {

                return number_format($order->rate, 8, '.' ,'');
            })
            ->make(true);
    }


    public function sell_orders($mainCode, $code)
    {
        $orders = cache()->tags('OrdersTableCommon')->remember('OrdersTableCommon.Sell.'.$code.$mainCode,  now()->addMinutes(10), function () use ($code, $mainCode)   {
            return ExchangeOrder::query()->select(DB::raw('rate, sum(amount) as sum'))->active()

                ->where('currency_id', Currency::getByCode($code)->id)
                ->where('main_currency_id', Currency::getByCode($mainCode)->id)
                ->where('type', ExchangeOrder::TYPE_SELL)
                ->groupBy('rate')
                ->orderBy('rate', 'asc')
                ->get();
        });

        return DataTables::of($orders)
            ->addColumn('num_rate', function (ExchangeOrder $order) {

                return number_format($order->rate, 8, '.' ,'');
            })
            ->make(true);
    }


    /**
     * @param DatatablesRequest $request
     * @param string $mainCode
     * @param string $code
     * @return mixed
     * @throws \Exception
     */
    public function my_orders(DatatablesRequest $request, $mainCode = 'FST', $code='WEC')
    {
        $order = $request->getOrder('id', 'desc');

        $myOrders = user()
            ->exchangeOrders()
            ->where('currency_id', Currency::getByCode($code)->id)
            ->where('main_currency_id', Currency::getByCode($mainCode)->id)
            ->orderBy($order['column'], $order['direction'])
            ->paginate($request->getPerPage(), $columns = ['*'], $pageName = 'page', $request->getPage());

        return DataTables::of($myOrders->items())
            ->skipPaging()
            ->with([
                'recordsTotal'  => $myOrders->total(),
                'recordsFiltered' => $myOrders->total(),
                'draw' => $request->draw
            ])
            ->addColumn('main_currency_code', function (ExchangeOrder $order) {
                return getCurrencyById($order->main_currency_id)->code;
            })
            ->addColumn('currency_code', function (ExchangeOrder $order) {
                return getCurrencyById($order->currency_id)->code;
            })
            ->addColumn('new_amount', function (ExchangeOrder $order) {
                return number_format( $order->amount,8);
            })
            ->addColumn('value', function (ExchangeOrder $order) {
                return number_format($order->amount*$order->rate, 8);
            })
            ->make(true);
    }






    /**
     * @param RequestExchangeCommon $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exchange(RequestExchangeCommon $request)
    {

        $data = cache()->pull('protect-exchange-'.getUserId());

        if ($data!==null)
        {

            return back()->with('error', __('Слишком много запросов'));
        }

        cache()->put('protect-exchange-'.getUserId(), '1', now()->addSeconds(10));
        $user = user();

        /**
         * @var Wallet $main_wallet
         */
        $main_wallet = $user->wallets()->find($request->main_wallet_id);
        $main_currency = getCurrencyById($main_wallet->currency_id);
        /**
         * @var Wallet $wallet
         */
        $wallet = $user->wallets()->find($request->wallet_id);
        $currency = getCurrencyById($wallet->currency_id);


        if ($request->type==ExchangeOrder::TYPE_SELL&&$main_currency->code === 'WEC')
        {
            if(!$user->hasRole(['root']))
            {
                if (!$user->deposits()->where('active', false)->exists())
                {
                    return back()->with('error', __('У вас нет закрытого депозита!'));
                }
            }


        }

//        if ($main_wallet->currency->code=='ACC'&&$wallet->currency->code=='WEC')
//        {
//                return back()->with('error', __('Sell ACC Closed'));
//        }

        OrderRequest::addRequest(
            $main_wallet,
            $main_currency,
            $wallet,
            $currency,
            $request->amount,
            $request->type,
            round($request->rate, 8)
        );

        return back()->with('success', __('Request created successful'));
    }


    /**
     * @param  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function close_order(Request $request, $id)
    {

        /**
         * @var ExchangeOrder $order
         */


        $data = cache()->pull('protect-exchange-'.getUserId());

        if ($data!==null)
        {

            return back()->with('error', __('Error'));
        }

        cache()->put('protect-exchange-'.getUserId(), '1', 5);



        $order = user()->exchangeOrders()->where('id', $id)->where('active',1)->first();



        if ($order===null)
        {
            return back()->with('error', __('Order not found!'));
        }

        $order->close();

        return back()->with('success', __('Order closed successful!'));

    }


    public function dataTableExchange(DatatablesRequest $request, $mainCode, $code)
    {
        $order = $request->getOrder();

        $operations = OrderPiece::where('user_id', getUserId())
            ->where('currency_id', Currency::getByCode($code)->id)
            ->where('main_currency_id', Currency::getByCode($mainCode)->id)
            ->orderBy($order['column'], $order['direction'])
            ->paginate($request->getPerPage(), $columns = ['*'], $pageName = 'page', $request->getPage());

        return DataTables::of($operations->items())
            ->skipPaging()
            ->with([
                'recordsTotal'  => $operations->total(),
                'recordsFiltered' => $operations->total(),
                'draw' => $request->draw
            ])
            ->addColumn('main_currency_code', function (OrderPiece $order) {
                return getCurrencyById($order->main_currency_id)->code;
            })
            ->addColumn('currency_code', function (OrderPiece $order) {
                return getCurrencyById($order->currency_id)->code;
            })
            ->make(true);
    }

    public function dataTableExchangeAll(DatatablesRequest $request, $mainCode, $code)
    {
        $order = $request->getOrder();

        $filters = [
            'currency' => $code,
            'main_currency' => $mainCode,
            'order' => $order,
            'pagination' => $request->getPagination()
        ];

        $operations = cache()->remember('tableExchangeAll.' . md5(serialize($filters)), now()->addMinute(), function () use ($filters) {
            return OrderPiece::query()
                ->where('currency_id', Currency::getByCode($filters['currency'])->id)
                ->where('main_currency_id', Currency::getByCode($filters['main_currency'])->id)
                ->orderBy($filters['order']['column'], $filters['order']['direction'])
                ->paginate($filters['pagination']['per_page'], $columns = ['*'], $pageName = 'page', $filters['pagination']['page']);
        });
        return Datatables::of($operations->items())
            ->skipPaging()
            ->with([
                'recordsTotal'  => $operations->total(),
                'recordsFiltered' => $operations->total(),
                'draw' => $request->draw
            ])
            ->addColumn('main_currency_code', function (OrderPiece $order) {
                return getCurrencyById($order->main_currency_id)->code;
            })
            ->addColumn('currency_code', function (OrderPiece $order) {
                return getCurrencyById($order->currency_id)->code;
            })
            ->make(true);
    }
}
