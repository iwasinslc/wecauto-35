<?php
namespace App\Http\Controllers\Admin;

use App\Console\Commands\Automatic\ScriptCheckerCommand;
use App\Events\NotificationEvent;
use App\Facades\WithdrawF;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Controller;
use App\Models\Currency;
use App\Models\PaymentSystem;
use App\Models\Transaction;
use App\Models\TransactionStatus;
use App\Models\TransactionType;
use App\Models\User;
use App\Models\Wallet;
use App\Models\Withdraw;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Yajra\Datatables\Datatables;

/**
 * Class WithdrawalRequestsController
 * @package App\Http\Controllers\Admin
 */
class WithdrawalRequestsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.requests.index');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function dataTable()
    {
        $wrs = Withdraw::select('withdraws.*')->with([
            'currency',
            'paymentSystem',
            'wallet',
            'user',
            'status'
        ])
            ->whereIn('status_id', [TransactionStatus::STATUS_CONFIRMED_BY_EMAIL, TransactionStatus::STATUS_ERROR]); // Get all not approved

        return Datatables::of($wrs)->editColumn('status', function ($wr) {
            return $wr->status->name;
        })->editColumn('amount', function (Withdraw $wr) {
            return number_format($wr->amount, $wr->wallet->currency->precision, '.', '');
        })->addColumn('external', function (Withdraw $wr) {
            return $wr->user->wallets()->where('payment_system_id', $wr->payment_system_id)->where('currency_id', $wr->currency_id)
                ->first()->external;
        })->make(true);
    }

    public function all()
    {
        return view('admin.requests.all');
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function dataTableAll()
    {
        $wrs = Withdraw::select('withdraws.*')->with([
            'currency',
            'paymentSystem',
            'wallet',
            'user',
            'status'
        ]);

        return Datatables::of($wrs)->editColumn('status', function ($wr) {
            return $wr->status->name;
        })->editColumn('amount', function (Withdraw $wr) {
            return number_format($wr->amount, $wr->wallet->currency->precision, '.', '');
        })->addColumn('external', function (Withdraw $wr) {
            return $wr->user->wallets()->where('payment_system_id', $wr->payment_system_id)->where('currency_id', $wr->currency_id)
                ->first()->external;
        })->make(true);
    }

    /**
     * @param $transaction
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($transaction)
    {
        $transaction = Withdraw::find($transaction);
        if(empty($transaction)) {
            return back()->with('error', __('Transaction not found'));
        }
        return view('admin.requests.show', [
            'transaction' => $transaction,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function approveMany(Request $request)
    {
        $messages = [];

        if(empty($request->list)) {
            return back()->with('error', __('Empty items list'));
        }

        if ($request->approve) {
            foreach ($request->list as $item) {
                $messages[] = $this->approve($item, true);
            }
        } elseif ($request->approveManually) {
            foreach ($request->list as $item) {
                $messages[] = $this->approveManually($item, true);
            }
        } elseif ($request->reject) {
            foreach ($request->list as $item) {
                $messages[] = $this->reject($item, true);
            }
        }

        return back()->with('success', __('List of withdrawal requests processed.').'<hr>'.implode('<hr>', $messages));
    }

    /**
     * @param $transaction
     * @param bool $massMode
     * @return array|\Illuminate\Http\RedirectResponse|null|string
     * @throws \Exception
     */
    public function reject($transaction, $massMode=false)
    {
        try {
            WithdrawF::reject($transaction);
            $status = 'success';
            $message = __('Request rejected');
        } catch (\Throwable $e) {
            $status = 'error';
            $message = $e->getMessage();
        }

        if (true === $massMode) {
            return $message;
        }
        return back()->with($status, $message);
    }

    /**
     * @param $transaction_id
     * @param bool $massMode
     * @return \Illuminate\Http\RedirectResponse|string
     * @throws \Exception
     */
    public static function approve($transaction_id, $massMode=false)
    {
        try {
            /** @var Withdraw $transaction */
            $transaction = Withdraw::find($transaction_id);

            if(empty($transaction))
                throw new \Exception('Withdraw not found: ' . $transaction_id);

            WithdrawF::approve($transaction);
            $status = 'success';
            $message = $transaction->amount.$transaction->currency->symbol.' - '.__('Request approved, money transferred to user wallet');
        } catch (ValidationException $e) {
            $status = 'error';
            $errors = $e->errors();
            $message = '';
            foreach ($errors as $messages) {
                foreach ($messages as $item) {
                    $message .= $item . '. ';
                }
            }
        } catch (\Throwable $e) {
            $status = 'error';
            Log::error($e->getMessage(), $e->getTrace());
            $message = $e->getMessage();
        }
        if($massMode) {
            return $message;
        } else {
            return back()->with($status, $message);
        }
    }

    /**
     * @param $transaction
     * @param bool $massMode
     * @return \Illuminate\Http\RedirectResponse|string
     * @throws \Exception
     */
    public function approveManually($transaction, $massMode=false)
    {
        /** @var Withdraw $transaction */
        $transaction = Withdraw::find($transaction);

        if ($transaction->isApproved()) {
            if (true === $massMode) {
                return __('This request already processed.');
            }
            return back()->with('error', __('This request already processed.'));
        }

        /** @var Wallet $wallet */
        $wallet         = $transaction->wallet()->first();
        /** @var User $user */
        $user           = $wallet->user()->first();
        /** @var PaymentSystem $paymentSystem */
        $paymentSystem  = $transaction->paymentSystem()->first();
        /** @var Currency $currency */
        $currency       = $wallet->currency()->first();

        if (null === $wallet || null === $user || null === $paymentSystem || null === $currency) {
            throw new \Exception('Wallet, user, payment system or currency is not found for withdrawal approve.');
        }

//        if (empty($wallet->external)) {
//            if (true === $massMode) {
//                return 'ERROR: wallet is empty';
//            }
//            return back()->with('error', 'ERROR: wallet is empty');
//        }

        $transaction->update([
            'status_id' => TransactionStatus::STATUS_APPROVED
        ]);

        NotificationEvent::dispatch($user, 'notifications.approved_withdrawal', [
            //'id'=>$user->id,
            'user_id'=>$user->id,
            'amount'=>$transaction->amount,
            'currency'=>$currency->code,

        ]);

        $ps = $paymentSystem->getClassInstance();

        try {
            $ps->getBalances();
        } catch (\Exception $e) {
            if (true === $massMode) {
                return 'ERROR: ' . $e->getMessage();
            }
            return back()->with('error', 'ERROR: ' . $e->getMessage());
        }

        if (true === $massMode) {
            return $transaction->amount.$currency->symbol.' - '.__('Request approved.');
        }
        return back()->with('success', $transaction->amount.$currency->symbol.' - '.__('Request approved.'));
    }
}
